#include "threads.h"

void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}

void call_I_Love_Threads()
{
	thread t(I_Love_Threads);
	t.join();
}

void printVector(vector<int> primes)
{
	vector<int>::const_iterator it;
	for (it = primes.begin(); it != primes.end(); ++it)
		cout << *it << endl;
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	bool prime;
	if (begin < 2)
	{
		begin = 2;
	}
	for (int i = begin; i<end; i++)
	{
		prime = true;
		for (int j = 2; j*j <= i; j++)
		{
			if (i % j == 0)
			{
				prime = false;
				break; 
			}
		}
		if(prime)
		{
			primes.push_back(i);
		}
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> a;
	clock_t time = clock();
	thread t(getPrimes, begin, end, ref(a));
	t.join();
	cout << "callGetPrimes took " << clock() - time << " milliseconds to run" << endl;
	return a;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	vector<int> a;
	vector<int>::const_iterator it;
	a = callGetPrimes(begin, end);
	for (it = a.begin(); it != a.end(); ++it)
		file << *it << endl;

}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	int size = ((end - begin) / N);
	ofstream file;
	file.open(filePath);
	if (!(file.is_open()))
	{
		cout << "failed to open file" << endl;
		return;
	}
	thread *threads = new thread[N];
	for (int i = 0; i < N; i ++)
	{
		threads[i] = thread(writePrimesToFile, (i*size), ((i*size) + size), ref(file));
		
	}
	for (int i = 0; i < N; i++)
	{
		threads[i].join();
	}
	file.close();
}
